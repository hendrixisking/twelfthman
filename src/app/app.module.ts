import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';

import { HttpClientModule } from '@angular/common/http'

import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';

import { ImagegalleryComponent } from './imagegallery/imagegallery.component';

import { AppComponent } from './app.component';
import { ImagegalleryFeedService } from './imagegallery/imagegallery-feed.service';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    ImagegalleryComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [ImagegalleryFeedService],
  bootstrap: [AppComponent]
})
export class AppModule { }
