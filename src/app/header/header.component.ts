
import { Component, OnDestroy } from '@angular/core';


@Component({
  selector: 'header',
  templateUrl: './header.component.html'
})

export class HeaderComponent implements OnDestroy {

	title = 'My Library';

	constructor() {}

	ngOnDestroy() {}

}
