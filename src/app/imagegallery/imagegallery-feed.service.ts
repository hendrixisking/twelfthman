import { Injectable } from '@angular/core';

import { RestService } from '../shared/service/rest.service';
import { Observable } from 'rxjs/Observable'; 
import { HttpClient } from '@angular/common/http';



@Injectable()

export class ImagegalleryFeedService {

  /*******JSON CALL FROM SERVER*********
  localhostName = 'localhost:8080';

  results: string[];
 
  // Inject HttpClient into your component or service.
  constructor(private http: HttpClient) {}
 
  ngOnInit(): void {
    // Make the HTTP request:
    this.http.get( this.localhostName + '/json/images.json').subscribe(data => {
      // Read the result field from the JSON response.
      this.results = data['results'];
    });
  }
  */


  /*******INSTEAD OF JSON CALL HERE IS AN ARRAY*********/

  private galleries = [
            {id: '0', image: 'image-0.jpg', description: 'Description of image 0', status: false },
            {id: '1', image: 'image-1.jpg', description: 'Description of image 1', status: true },
            {id: '2', image: 'image-2.jpg', description: 'Description of image 2', status: true },
            {id: '3', image: 'image-3.jpg', description: 'Description of image 3', status: true },
            {id: '4', image: 'image-4.jpg', description: 'Description of image 4', status: true },
            {id: '5', image: 'image-5.jpg', description: 'Description of image 5', status: true }
        ]

  getImages() {
  	return this.galleries;
  }
   
   

}
