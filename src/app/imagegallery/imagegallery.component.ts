import { Component, OnInit } from '@angular/core';
import { GalleryItem } from '../shared/GalleryItem';
import { ImagegalleryFeedService } from './imagegallery-feed.service';

@Component({
  selector: 'imagegallery',
  templateUrl: './imagegallery.component.html',
  styleUrls: ['./imagegallery.component.css']
})




export class ImagegalleryComponent implements OnInit {

  activeTab = 'active'; //values: active or deleted
  selectedGalleryItemId: number;
  galleries: GalleryItem[];
  downloadLink = ''

    constructor(service: ImagegalleryFeedService) {
      this.galleries = service.getImages();
    }

    downloadImage() {
    }

    deleteImage() {
      this.galleries[ this.selectedGalleryItemId ].status = false;
      console.log(this.galleries);
    }

    restoreImage() {
      this.galleries[ this.selectedGalleryItemId ].status = true;
    }

    selectImage(imageId) {
      this.selectedGalleryItemId = imageId;
      this.downloadLink = './assets/images/' + this.galleries[ this.selectedGalleryItemId ].image;
      console.log(this.selectedGalleryItemId);
    }

    deselectImage() {
      this.selectedGalleryItemId = 0;
      console.log(this.selectedGalleryItemId);
    }

        
    showPanel(panel: string) {
        this.activeTab = panel;
        console.log(this.activeTab);
        this.deselectImage();
    }

  ngOnInit() {
  }

}
