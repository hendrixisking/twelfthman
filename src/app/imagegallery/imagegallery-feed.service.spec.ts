import { TestBed, inject } from '@angular/core/testing';

import { ImagegalleryFeedService } from './imagegallery-feed.service';

describe('ImagegalleryFeedService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ImagegalleryFeedService]
    });
  });

  it('should be created', inject([ImagegalleryFeedService], (service: ImagegalleryFeedService) => {
    expect(service).toBeTruthy();
  }));
});
