export interface GalleryItem {
    id?;
    image?;
    description?;
    status?;
}